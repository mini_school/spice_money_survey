import 'package:flutter/material.dart';
import 'package:spice_money/presentation/success_screen/success_screen.dart';
import 'package:spice_money/spice_money.dart';

class RouteGenrator {
  static Route<dynamic> genrateRoute(RouteSettings routeSettings) {
    //final args = routeSettings.arguments;
    switch (routeSettings.name) {
      case '/':
      case WelcomeScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => BlocProvider<WelcomeScreenBloc>(
            create: (_) => sLocator.get<WelcomeScreenBloc>(),
            child: WelcomeScreen(),
          ),
        );
      case DynamicFormScreen.routeName:
        GetInfoApiResp getInfoApiResp =
            routeSettings.arguments as GetInfoApiResp;
        return MaterialPageRoute(
          builder: (_) => BlocProvider<DynamicFormScreenBloc>(
            create: (context) => sLocator.get<DynamicFormScreenBloc>(),
            child: DynamicFormScreen(getInfoApiResp: getInfoApiResp),
          ),
        );
      case SuccessScreen.routeName:
        var msg = routeSettings.arguments as String;
        return MaterialPageRoute(
          builder: (_) => SuccessScreen(
            msg: msg,
          ),
        );
      default:
        return MaterialPageRoute<dynamic>(
          builder: (_) => UnknownScreen(),
        );
    }
  }
}
