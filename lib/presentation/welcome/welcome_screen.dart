import 'package:flutter/material.dart';
import 'package:spice_money/presentation/dynamic_form/dynamic_form_screen.dart';
import 'package:spice_money/spice_money.dart';

class WelcomeScreen extends StatefulWidget {
  static const routeName = "/WelcomeScreen";

  WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _WelcomeScreen();
  }
}

class _WelcomeScreen extends State<WelcomeScreen> {
  @override
  void initState() {
    context.read<WelcomeScreenBloc>()..add(WelcomeScreenEvent.initalized());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<WelcomeScreenBloc, WelcomeScreenState>(
          builder: (context, state) {
            if (state.isLoading == true) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state.isLoaded == true && state.getInfoApiResp != null) {
              return Column(
                children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            state.getInfoApiResp!.welcomeScreen!.title!,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            state.getInfoApiResp!.welcomeScreen!.description!,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.black)),
                        child: Text(
                          'Start',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, DynamicFormScreen.routeName,
                              arguments: state.getInfoApiResp);
                        },
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }
}
