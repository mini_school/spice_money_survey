import 'package:flutter/material.dart';
import 'package:spice_money/spice_money.dart';

class UnknownScreen extends StatelessWidget with RouteWrapper {
  final UnknownScreenArguments? arguments;
  UnknownScreen({Key? key, this.arguments}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('${arguments?.notFound}',
                          style: Theme.of(context).textTheme.headline6),
                      Text(
                        'not found',
                        style: Theme.of(context).textTheme.bodyText2,
                      )
                    ]),
                Text('Unknown route',
                    style: Theme.of(context).textTheme.caption)
              ]),
        ),
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) {
    return this;
  }
}

class UnknownScreenArguments extends ScreenArguments {
  final String? notFound;
  UnknownScreenArguments({this.notFound});
}
