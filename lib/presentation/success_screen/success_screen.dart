import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:spice_money/presentation/index.dart';

class SuccessScreen extends StatelessWidget {
  static const routeName = "/SuccessScreen";
  final String? msg;
  const SuccessScreen({Key? key, this.msg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Text(
                    msg!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.black)),
                    child: Text(
                      'Again',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.pushReplacementNamed(
                        context,
                        WelcomeScreen.routeName,
                      );
                    },
                  ),
                  InkWell(
                    child: Icon(Icons.share),
                    onTap: () {
                      Share.share(msg!);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
