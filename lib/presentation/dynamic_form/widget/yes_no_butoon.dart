import 'package:flutter/material.dart';

class YesNoButton extends StatefulWidget {
  final String? title;
  final Function(bool answer)? onChange;
  YesNoButton({
    Key? key,
    required this.title,
    required this.onChange,
  }) : super(key: key);

  @override
  _YesNoButtonState createState() => _YesNoButtonState();
}

class _YesNoButtonState extends State<YesNoButton> {
  List<bool> _selection = List.generate(2, (_) => false);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(widget.title!),
        SizedBox(
          height: 10,
        ),
        ToggleButtons(
          borderColor: Colors.black,
          fillColor: Colors.green[200],
          borderWidth: 2,
          selectedBorderColor: Colors.black,
          selectedColor: Colors.black,
          borderRadius: BorderRadius.circular(0),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'yes',
                style: TextStyle(fontSize: 16),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'No',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
          onPressed: (int index) {
            setState(() {
              if (index == 0) {
                _selection[0] = true;
                _selection[1] = false;
                widget.onChange!(true);
              } else {
                _selection[0] = false;
                _selection[1] = true;
                widget.onChange!(false);
              }
            });
          },
          isSelected: _selection,
        ),
      ],
    );
  }
}
