import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:spice_money/spice_money.dart';

class FormFieldBuilderRatingBar extends FormBuilderField<double> {
  final String? title;
  final int? maxRateing;
  final double? initialRating;
  FormFieldBuilderRatingBar({
    required String name,
    required this.title,
    required this.maxRateing,
    this.initialRating = 0.0,
    Key? key,
    FormFieldValidator<double>? validator,
    InputDecoration decoration = const InputDecoration(
      border: InputBorder.none,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
    ),
    AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
    FocusNode? focusNode,
    ValueChanged<double?>? onChanged,
    ValueTransformer<double?>? valueTransformer,
    FormFieldSetter<double?>? onSaved,
  }) : super(
            name: name,
            onChanged: onChanged,
            decoration: decoration,
            focusNode: focusNode,
            onSaved: onSaved,
            validator: validator,
            valueTransformer: valueTransformer,
            builder: (field) {
              final state = field as _FormFieldBuilderRatingBarState;
              return InputDecorator(
                decoration: state.decoration(),
                child: CustomRatingBar(
                    title: title,
                    maxRating: maxRateing,
                    initialRating: 0.0,
                    onRatingChange: (val) {
                      state.requestFocus();
                      state.didChange(val);
                    }),
              );
            });
  @override
  _FormFieldBuilderRatingBarState createState() =>
      _FormFieldBuilderRatingBarState();
}

class _FormFieldBuilderRatingBarState
    extends FormBuilderFieldState<FormFieldBuilderRatingBar, double> {}
