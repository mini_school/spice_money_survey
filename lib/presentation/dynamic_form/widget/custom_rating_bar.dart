import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class CustomRatingBar extends StatelessWidget {
  final String? title;
  final int? maxRating;
  final double? initialRating;
  final Function(double rating)? onRatingChange;
  const CustomRatingBar({
    this.title,
    this.maxRating,
    Key? key,
    this.initialRating,
    this.onRatingChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(title!),
          SizedBox(
            height: 10,
          ),
          RatingBar.builder(
            initialRating: initialRating!,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemSize: 30,
            itemCount: maxRating!,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.green,
            ),
            onRatingUpdate: (rating) {
              print(rating);
              onRatingChange!(rating);
            },
          ),
        ],
      ),
    );
  }
}
