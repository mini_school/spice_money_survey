import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:spice_money/spice_money.dart';

class FormFieldBuilderYesNoButton extends FormBuilderField<bool> {
  final String? title;
  final String? errorMsg;
  FormFieldBuilderYesNoButton({
    required String name,
    required this.title,
    this.errorMsg,
    Key? key,
    FormFieldValidator<bool>? validator,
    InputDecoration decoration = const InputDecoration(
      border: InputBorder.none,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
    ),
    AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
    FocusNode? focusNode,
    ValueChanged<bool?>? onChanged,
    ValueTransformer<bool?>? valueTransformer,
    FormFieldSetter<bool?>? onSaved,
  }) : super(
            name: name,
            onChanged: onChanged,
            decoration: decoration,
            focusNode: focusNode,
            onSaved: onSaved,
            validator: validator,
            valueTransformer: valueTransformer,
            builder: (field) {
              final state = field as _FormFieldBuilderYesNoButtonState;
              return InputDecorator(
                decoration: state.decoration(),
                child: (YesNoButton(
                    title: title,
                    onChange: (val) {
                      state.requestFocus();
                      state.didChange(val);
                    })),
              );
            });
  @override
  _FormFieldBuilderYesNoButtonState createState() =>
      _FormFieldBuilderYesNoButtonState();
}

class _FormFieldBuilderYesNoButtonState
    extends FormBuilderFieldState<FormFieldBuilderYesNoButton, bool> {}
