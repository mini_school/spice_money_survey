import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:spice_money/presentation/success_screen/success_screen.dart';
import 'package:ndialog/ndialog.dart';

import 'package:spice_money/spice_money.dart';

class DynamicFormScreen extends StatelessWidget {
  static const routeName = "/DynamicFormScreen";
  final GetInfoApiResp? getInfoApiResp;
  DynamicFormScreen({Key? key, this.getInfoApiResp}) : super(key: key);
  final _formKey = GlobalKey<FormBuilderState>();

  Future<bool> _onWillpop() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog? progressDialog;

    return Scaffold(
      appBar: AppBar(
        title: Text('Survey'),
      ),
      body: BlocListener<DynamicFormScreenBloc, DynamicFormScreenState>(
        listener: (context, state) {
          if (state.isSuccess == true) {
            progressDialog?.dismiss();
            Navigator.pushReplacementNamed(context, SuccessScreen.routeName,
                arguments: getInfoApiResp!.thankyouScreens?.title);
          }
          if (state.isLoading == true) {
            progressDialog = ProgressDialog(
              context,
              message: Text('Loading...'),
              title: Text(AppContant.appName),
            );
            progressDialog?.show();
          } else if (state.isLoaded == true) {
            progressDialog?.dismiss();
          } else if (state.isError == true) {
            progressDialog?.dismiss();
            Fluttertoast.showToast(msg: 'Opps! something went Wrong');
          }
        },
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(10),
                  child: FormBuilder(
                    key: _formKey,
                    onWillPop: _onWillpop,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          child: getDynamicWidget(
                              context, getInfoApiResp!.fields![index]!),
                        );
                      },
                      itemCount: getInfoApiResp!.fields!.length,
                      shrinkWrap: true,
                    ),
                  ),
                ),
                Center(
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.black)),
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      _formKey.currentState!.save();
                      if (_formKey.currentState!.validate()) {
                        print(_formKey.currentState!.value);
                        context.read<DynamicFormScreenBloc>()
                          ..add(DynamicFormScreenEvent.sendDataRequest(
                              value: _formKey.currentState!.value));
                      } else {
                        print("validation failed");
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget getDynamicWidget(
    BuildContext context, GetInfoApiRespFields getInfoApiRespFields) {
  switch (getInfoApiRespFields.type!) {
    case 'short_text':
      return FormBuilderTextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        name: getInfoApiRespFields.id!,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
          FormBuilderValidators.maxLength(context, 15)
        ]),
      );

    case 'dropdown':
      var option = getInfoApiRespFields.properties!.choices!;
      if (getInfoApiRespFields.properties!.alphabeticalOrder == true) {
        option.sort((a, b) => a!.label!.compareTo(b!.label!));
      }
      return FormBuilderDropdown(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        name: getInfoApiRespFields.id!,
        items: option
            .map((e) => DropdownMenuItem(
                  child: Text('${e!.label}'),
                  value: e.label,
                ))
            .toList(),
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
        ]),
      );

    case 'number':
      return FormBuilderTextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        name: getInfoApiRespFields.id!,
        keyboardType: TextInputType.number,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
          FormBuilderValidators.maxLength(context, 5),
          FormBuilderValidators.numeric(context)
        ]),
      );

    case 'email':
      return FormBuilderTextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        name: getInfoApiRespFields.id!,
        keyboardType: TextInputType.emailAddress,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.email(context),
        ]),
      );

    case 'phone_number':
      return FormBuilderTextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        name: getInfoApiRespFields.id!,
        keyboardType: TextInputType.phone,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
        ]),
      );

    case 'date':
      return FormBuilderDateTimePicker(
        name: getInfoApiRespFields.id!,
        // onChanged: _onChanged,
        inputType: InputType.date,

        decoration: InputDecoration(
          prefixIcon: Icon(Icons.date_range),
          border: OutlineInputBorder(),
          labelText: getInfoApiRespFields.title!,
          hintText: getInfoApiRespFields.title!,
        ),
        initialTime: TimeOfDay(hour: 8, minute: 0),
        // initialValue: DateTime.now(),
        enabled: true,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context),
        ]),
      );

    case 'yes_no':
      return FormFieldBuilderYesNoButton(
        name: getInfoApiRespFields.id!,
        title: getInfoApiRespFields.title!,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context, errorText: 'Please Answer'),
        ]),
      );

    case 'rating':
      return FormFieldBuilderRatingBar(
        title: getInfoApiRespFields.title!,
        maxRateing: getInfoApiRespFields.properties!.steps!,
        name: getInfoApiRespFields.id!,
        validator: FormBuilderValidators.compose([
          if (getInfoApiRespFields.validations!.required == true)
            FormBuilderValidators.required(context,
                errorText: "Please rate your experience"),
        ]),
      );

    default:
      return Container();
  }
}
