import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:spice_money/core/api_service/network_api.dart';
import 'package:spice_money/domain/index.dart';

class GetInfoRepository extends IGetInfoRepository {
  final NetworkApi networkApi;
  GetInfoRepository(this.networkApi);

  @override
  Future<Either<NetworkFailure, GetInfoApiResp>> getInformation() async {
    try {
      var resp = await networkApi.getInformation();
      return right(resp);
    } on FormatException {
      return left(NetworkFailure.unexpected());
    } on DioError catch (e) {
      return left(NetworkFailure.apiError(e.error.toString()));
    }
  }

  @override
  Future<Either<NetworkFailure, FormResponse>> sendInformation(
      String body) async {
    try {
      var resp = await networkApi.uploadInformation(body);
      return right(resp);
    } on FormatException {
      return left(NetworkFailure.unexpected());
    } on DioError catch (e) {
      return left(NetworkFailure.apiError(e.error.toString()));
    }
  }
}
