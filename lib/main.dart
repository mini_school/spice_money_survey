import 'dart:async';
import 'dart:io';

import 'package:alice/alice.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:spice_money/route_genrator.dart';
import 'package:spice_money/spice_money.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  runZoned<Future<void>>(
    () async {
      await ServiceLocator.init();
      SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
      runApp(MyApp());
    },
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Spice Money',
        initialRoute: WelcomeScreen.routeName,
        onGenerateRoute: RouteGenrator.genrateRoute,
        debugShowCheckedModeBanner: false,
        navigatorKey: sLocator.get<Alice>().getNavigatorKey());
  }
}
