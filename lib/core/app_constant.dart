class AppContant {
  AppContant._();
  static const DEFAULT_CONNECTION_TIMEOUT = const Duration(seconds: 10);
  static const DEFAULT_RECEIVE_TIMEOUT = const Duration(seconds: 10);
  static const BaseUrl = 'http://getx-todo-server.herokuapp.com';
  static const String appName = "Spice Money";
}
