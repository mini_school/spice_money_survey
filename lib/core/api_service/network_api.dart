import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:spice_money/domain/index.dart';

part 'network_api.g.dart';

@RestApi()
abstract class NetworkApi {
  factory NetworkApi(Dio dio, {String? baseUrl}) = _NetworkApi;

  @GET('')
  Future<GetInfoApiResp> getInformation();

  @POST('')
  Future<FormResponse> uploadInformation(@Body() String body);
}
