import 'package:flutter/material.dart';

mixin RouteWrapper {
  Widget wrappedRoute(BuildContext context);
}
