// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'welcome_screen_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$WelcomeScreenEventTearOff {
  const _$WelcomeScreenEventTearOff();

  _Initalized initalized() {
    return const _Initalized();
  }
}

/// @nodoc
const $WelcomeScreenEvent = _$WelcomeScreenEventTearOff();

/// @nodoc
mixin _$WelcomeScreenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initalized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initalized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initalized value) initalized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initalized value)? initalized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WelcomeScreenEventCopyWith<$Res> {
  factory $WelcomeScreenEventCopyWith(
          WelcomeScreenEvent value, $Res Function(WelcomeScreenEvent) then) =
      _$WelcomeScreenEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$WelcomeScreenEventCopyWithImpl<$Res>
    implements $WelcomeScreenEventCopyWith<$Res> {
  _$WelcomeScreenEventCopyWithImpl(this._value, this._then);

  final WelcomeScreenEvent _value;
  // ignore: unused_field
  final $Res Function(WelcomeScreenEvent) _then;
}

/// @nodoc
abstract class _$InitalizedCopyWith<$Res> {
  factory _$InitalizedCopyWith(
          _Initalized value, $Res Function(_Initalized) then) =
      __$InitalizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitalizedCopyWithImpl<$Res>
    extends _$WelcomeScreenEventCopyWithImpl<$Res>
    implements _$InitalizedCopyWith<$Res> {
  __$InitalizedCopyWithImpl(
      _Initalized _value, $Res Function(_Initalized) _then)
      : super(_value, (v) => _then(v as _Initalized));

  @override
  _Initalized get _value => super._value as _Initalized;
}

/// @nodoc

class _$_Initalized implements _Initalized {
  const _$_Initalized();

  @override
  String toString() {
    return 'WelcomeScreenEvent.initalized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initalized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initalized,
  }) {
    return initalized();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initalized,
    required TResult orElse(),
  }) {
    if (initalized != null) {
      return initalized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initalized value) initalized,
  }) {
    return initalized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initalized value)? initalized,
    required TResult orElse(),
  }) {
    if (initalized != null) {
      return initalized(this);
    }
    return orElse();
  }
}

abstract class _Initalized implements WelcomeScreenEvent {
  const factory _Initalized() = _$_Initalized;
}

/// @nodoc
class _$WelcomeScreenStateTearOff {
  const _$WelcomeScreenStateTearOff();

  _WelcomeScreenState call(
      {bool? isLoaded,
      bool? isError,
      bool? isLoading,
      GetInfoApiResp? getInfoApiResp}) {
    return _WelcomeScreenState(
      isLoaded: isLoaded,
      isError: isError,
      isLoading: isLoading,
      getInfoApiResp: getInfoApiResp,
    );
  }
}

/// @nodoc
const $WelcomeScreenState = _$WelcomeScreenStateTearOff();

/// @nodoc
mixin _$WelcomeScreenState {
  bool? get isLoaded => throw _privateConstructorUsedError;
  bool? get isError => throw _privateConstructorUsedError;
  bool? get isLoading => throw _privateConstructorUsedError;
  GetInfoApiResp? get getInfoApiResp => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WelcomeScreenStateCopyWith<WelcomeScreenState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WelcomeScreenStateCopyWith<$Res> {
  factory $WelcomeScreenStateCopyWith(
          WelcomeScreenState value, $Res Function(WelcomeScreenState) then) =
      _$WelcomeScreenStateCopyWithImpl<$Res>;
  $Res call(
      {bool? isLoaded,
      bool? isError,
      bool? isLoading,
      GetInfoApiResp? getInfoApiResp});
}

/// @nodoc
class _$WelcomeScreenStateCopyWithImpl<$Res>
    implements $WelcomeScreenStateCopyWith<$Res> {
  _$WelcomeScreenStateCopyWithImpl(this._value, this._then);

  final WelcomeScreenState _value;
  // ignore: unused_field
  final $Res Function(WelcomeScreenState) _then;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? isError = freezed,
    Object? isLoading = freezed,
    Object? getInfoApiResp = freezed,
  }) {
    return _then(_value.copyWith(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool?,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool?,
      getInfoApiResp: getInfoApiResp == freezed
          ? _value.getInfoApiResp
          : getInfoApiResp // ignore: cast_nullable_to_non_nullable
              as GetInfoApiResp?,
    ));
  }
}

/// @nodoc
abstract class _$WelcomeScreenStateCopyWith<$Res>
    implements $WelcomeScreenStateCopyWith<$Res> {
  factory _$WelcomeScreenStateCopyWith(
          _WelcomeScreenState value, $Res Function(_WelcomeScreenState) then) =
      __$WelcomeScreenStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool? isLoaded,
      bool? isError,
      bool? isLoading,
      GetInfoApiResp? getInfoApiResp});
}

/// @nodoc
class __$WelcomeScreenStateCopyWithImpl<$Res>
    extends _$WelcomeScreenStateCopyWithImpl<$Res>
    implements _$WelcomeScreenStateCopyWith<$Res> {
  __$WelcomeScreenStateCopyWithImpl(
      _WelcomeScreenState _value, $Res Function(_WelcomeScreenState) _then)
      : super(_value, (v) => _then(v as _WelcomeScreenState));

  @override
  _WelcomeScreenState get _value => super._value as _WelcomeScreenState;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? isError = freezed,
    Object? isLoading = freezed,
    Object? getInfoApiResp = freezed,
  }) {
    return _then(_WelcomeScreenState(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool?,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool?,
      getInfoApiResp: getInfoApiResp == freezed
          ? _value.getInfoApiResp
          : getInfoApiResp // ignore: cast_nullable_to_non_nullable
              as GetInfoApiResp?,
    ));
  }
}

/// @nodoc

class _$_WelcomeScreenState implements _WelcomeScreenState {
  const _$_WelcomeScreenState(
      {this.isLoaded, this.isError, this.isLoading, this.getInfoApiResp});

  @override
  final bool? isLoaded;
  @override
  final bool? isError;
  @override
  final bool? isLoading;
  @override
  final GetInfoApiResp? getInfoApiResp;

  @override
  String toString() {
    return 'WelcomeScreenState(isLoaded: $isLoaded, isError: $isError, isLoading: $isLoading, getInfoApiResp: $getInfoApiResp)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _WelcomeScreenState &&
            (identical(other.isLoaded, isLoaded) ||
                const DeepCollectionEquality()
                    .equals(other.isLoaded, isLoaded)) &&
            (identical(other.isError, isError) ||
                const DeepCollectionEquality()
                    .equals(other.isError, isError)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.getInfoApiResp, getInfoApiResp) ||
                const DeepCollectionEquality()
                    .equals(other.getInfoApiResp, getInfoApiResp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoaded) ^
      const DeepCollectionEquality().hash(isError) ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(getInfoApiResp);

  @JsonKey(ignore: true)
  @override
  _$WelcomeScreenStateCopyWith<_WelcomeScreenState> get copyWith =>
      __$WelcomeScreenStateCopyWithImpl<_WelcomeScreenState>(this, _$identity);
}

abstract class _WelcomeScreenState implements WelcomeScreenState {
  const factory _WelcomeScreenState(
      {bool? isLoaded,
      bool? isError,
      bool? isLoading,
      GetInfoApiResp? getInfoApiResp}) = _$_WelcomeScreenState;

  @override
  bool? get isLoaded => throw _privateConstructorUsedError;
  @override
  bool? get isError => throw _privateConstructorUsedError;
  @override
  bool? get isLoading => throw _privateConstructorUsedError;
  @override
  GetInfoApiResp? get getInfoApiResp => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$WelcomeScreenStateCopyWith<_WelcomeScreenState> get copyWith =>
      throw _privateConstructorUsedError;
}
