part of 'welcome_screen_bloc.dart';

@freezed
class WelcomeScreenEvent with _$WelcomeScreenEvent {
  const factory WelcomeScreenEvent.initalized() = _Initalized;
}
