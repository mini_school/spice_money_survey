import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:spice_money/spice_money.dart';

part 'welcome_screen_event.dart';
part 'welcome_screen_state.dart';
part 'welcome_screen_bloc.freezed.dart';

class WelcomeScreenBloc extends Bloc<WelcomeScreenEvent, WelcomeScreenState> {
  final GetInfoRepository getInfoRepository;
  WelcomeScreenBloc({required this.getInfoRepository})
      : super(WelcomeScreenState.initialize());

  @override
  Stream<WelcomeScreenState> mapEventToState(
    WelcomeScreenEvent event,
  ) async* {
    yield* event.map(initalized: (value) async* {
      yield state.copyWith(isLoading: true, isLoaded: false);
      var resp = await getInfoRepository.getInformation();
      var data = resp.fold((l) => null, (r) => r);
      var isError = false;
      if (data == null) {
        isError = true;
      }
      yield state.copyWith(
          isLoading: false,
          isLoaded: true,
          isError: isError,
          getInfoApiResp: data);
    });
  }
}
