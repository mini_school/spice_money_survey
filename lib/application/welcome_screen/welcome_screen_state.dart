part of 'welcome_screen_bloc.dart';

@freezed
class WelcomeScreenState with _$WelcomeScreenState {
  const factory WelcomeScreenState({
    @required bool? isLoaded,
    bool? isError,
    bool? isLoading,
    GetInfoApiResp? getInfoApiResp,
  }) = _WelcomeScreenState;

  factory WelcomeScreenState.initialize() => WelcomeScreenState(
        isLoaded: true,
        isError: false,
        isLoading: false,
        getInfoApiResp: null,
      );
}
