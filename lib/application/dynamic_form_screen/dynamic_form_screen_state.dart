part of 'dynamic_form_screen_bloc.dart';

@freezed
class DynamicFormScreenState with _$DynamicFormScreenState {
  const factory DynamicFormScreenState({
    @required bool? isLoaded,
    bool? isError,
    bool? isLoading,
    bool? isSuccess,
  }) = _DynamicFormScreenState;
  factory DynamicFormScreenState.initialize() => DynamicFormScreenState(
        isLoaded: false,
        isError: false,
        isLoading: true,
        isSuccess: false,
      );
}
