import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:spice_money/spice_money.dart';

part 'dynamic_form_screen_event.dart';
part 'dynamic_form_screen_state.dart';
part 'dynamic_form_screen_bloc.freezed.dart';

class DynamicFormScreenBloc
    extends Bloc<DynamicFormScreenEvent, DynamicFormScreenState> {
  final GetInfoRepository getInfoRepository;
  DynamicFormScreenBloc({required this.getInfoRepository})
      : super(DynamicFormScreenState.initialize());

  @override
  Stream<DynamicFormScreenState> mapEventToState(
    DynamicFormScreenEvent event,
  ) async* {
    yield* event.map(
      sendDataRequest: (value) async* {
        yield state.copyWith(isLoading: true, isLoaded: false);
        List<FieldData> req = [];
        value.value.forEach((k, v) {
          var data = FieldData(fieldId: k, fieldData: v.toString());
          req.add(data);
        });
        print(req.toString());
        var respApi = await getInfoRepository.sendInformation(jsonEncode(req));
        var data = respApi.fold((l) => null, (r) => r);
        var isError = false;
        if (data == null) {
          isError = true;
        }
        yield state.copyWith(
            isLoading: false,
            isLoaded: true,
            isError: isError,
            isSuccess: !isError);
      },
    );
  }
}
