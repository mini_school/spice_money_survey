part of 'dynamic_form_screen_bloc.dart';

@freezed
class DynamicFormScreenEvent with _$DynamicFormScreenEvent {
  const factory DynamicFormScreenEvent.sendDataRequest(
      {required Map<String, dynamic> value}) = _SendDataRequest;
}
