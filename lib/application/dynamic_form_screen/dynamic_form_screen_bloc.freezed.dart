// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'dynamic_form_screen_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$DynamicFormScreenEventTearOff {
  const _$DynamicFormScreenEventTearOff();

  _SendDataRequest sendDataRequest({required Map<String, dynamic> value}) {
    return _SendDataRequest(
      value: value,
    );
  }
}

/// @nodoc
const $DynamicFormScreenEvent = _$DynamicFormScreenEventTearOff();

/// @nodoc
mixin _$DynamicFormScreenEvent {
  Map<String, dynamic> get value => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) sendDataRequest,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? sendDataRequest,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendDataRequest value) sendDataRequest,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendDataRequest value)? sendDataRequest,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DynamicFormScreenEventCopyWith<DynamicFormScreenEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DynamicFormScreenEventCopyWith<$Res> {
  factory $DynamicFormScreenEventCopyWith(DynamicFormScreenEvent value,
          $Res Function(DynamicFormScreenEvent) then) =
      _$DynamicFormScreenEventCopyWithImpl<$Res>;
  $Res call({Map<String, dynamic> value});
}

/// @nodoc
class _$DynamicFormScreenEventCopyWithImpl<$Res>
    implements $DynamicFormScreenEventCopyWith<$Res> {
  _$DynamicFormScreenEventCopyWithImpl(this._value, this._then);

  final DynamicFormScreenEvent _value;
  // ignore: unused_field
  final $Res Function(DynamicFormScreenEvent) _then;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc
abstract class _$SendDataRequestCopyWith<$Res>
    implements $DynamicFormScreenEventCopyWith<$Res> {
  factory _$SendDataRequestCopyWith(
          _SendDataRequest value, $Res Function(_SendDataRequest) then) =
      __$SendDataRequestCopyWithImpl<$Res>;
  @override
  $Res call({Map<String, dynamic> value});
}

/// @nodoc
class __$SendDataRequestCopyWithImpl<$Res>
    extends _$DynamicFormScreenEventCopyWithImpl<$Res>
    implements _$SendDataRequestCopyWith<$Res> {
  __$SendDataRequestCopyWithImpl(
      _SendDataRequest _value, $Res Function(_SendDataRequest) _then)
      : super(_value, (v) => _then(v as _SendDataRequest));

  @override
  _SendDataRequest get _value => super._value as _SendDataRequest;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_SendDataRequest(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc

class _$_SendDataRequest implements _SendDataRequest {
  const _$_SendDataRequest({required this.value});

  @override
  final Map<String, dynamic> value;

  @override
  String toString() {
    return 'DynamicFormScreenEvent.sendDataRequest(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SendDataRequest &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(value);

  @JsonKey(ignore: true)
  @override
  _$SendDataRequestCopyWith<_SendDataRequest> get copyWith =>
      __$SendDataRequestCopyWithImpl<_SendDataRequest>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) sendDataRequest,
  }) {
    return sendDataRequest(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? sendDataRequest,
    required TResult orElse(),
  }) {
    if (sendDataRequest != null) {
      return sendDataRequest(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendDataRequest value) sendDataRequest,
  }) {
    return sendDataRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendDataRequest value)? sendDataRequest,
    required TResult orElse(),
  }) {
    if (sendDataRequest != null) {
      return sendDataRequest(this);
    }
    return orElse();
  }
}

abstract class _SendDataRequest implements DynamicFormScreenEvent {
  const factory _SendDataRequest({required Map<String, dynamic> value}) =
      _$_SendDataRequest;

  @override
  Map<String, dynamic> get value => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$SendDataRequestCopyWith<_SendDataRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$DynamicFormScreenStateTearOff {
  const _$DynamicFormScreenStateTearOff();

  _DynamicFormScreenState call(
      {bool? isLoaded, bool? isError, bool? isLoading, bool? isSuccess}) {
    return _DynamicFormScreenState(
      isLoaded: isLoaded,
      isError: isError,
      isLoading: isLoading,
      isSuccess: isSuccess,
    );
  }
}

/// @nodoc
const $DynamicFormScreenState = _$DynamicFormScreenStateTearOff();

/// @nodoc
mixin _$DynamicFormScreenState {
  bool? get isLoaded => throw _privateConstructorUsedError;
  bool? get isError => throw _privateConstructorUsedError;
  bool? get isLoading => throw _privateConstructorUsedError;
  bool? get isSuccess => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DynamicFormScreenStateCopyWith<DynamicFormScreenState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DynamicFormScreenStateCopyWith<$Res> {
  factory $DynamicFormScreenStateCopyWith(DynamicFormScreenState value,
          $Res Function(DynamicFormScreenState) then) =
      _$DynamicFormScreenStateCopyWithImpl<$Res>;
  $Res call({bool? isLoaded, bool? isError, bool? isLoading, bool? isSuccess});
}

/// @nodoc
class _$DynamicFormScreenStateCopyWithImpl<$Res>
    implements $DynamicFormScreenStateCopyWith<$Res> {
  _$DynamicFormScreenStateCopyWithImpl(this._value, this._then);

  final DynamicFormScreenState _value;
  // ignore: unused_field
  final $Res Function(DynamicFormScreenState) _then;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? isError = freezed,
    Object? isLoading = freezed,
    Object? isSuccess = freezed,
  }) {
    return _then(_value.copyWith(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool?,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool?,
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$DynamicFormScreenStateCopyWith<$Res>
    implements $DynamicFormScreenStateCopyWith<$Res> {
  factory _$DynamicFormScreenStateCopyWith(_DynamicFormScreenState value,
          $Res Function(_DynamicFormScreenState) then) =
      __$DynamicFormScreenStateCopyWithImpl<$Res>;
  @override
  $Res call({bool? isLoaded, bool? isError, bool? isLoading, bool? isSuccess});
}

/// @nodoc
class __$DynamicFormScreenStateCopyWithImpl<$Res>
    extends _$DynamicFormScreenStateCopyWithImpl<$Res>
    implements _$DynamicFormScreenStateCopyWith<$Res> {
  __$DynamicFormScreenStateCopyWithImpl(_DynamicFormScreenState _value,
      $Res Function(_DynamicFormScreenState) _then)
      : super(_value, (v) => _then(v as _DynamicFormScreenState));

  @override
  _DynamicFormScreenState get _value => super._value as _DynamicFormScreenState;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? isError = freezed,
    Object? isLoading = freezed,
    Object? isSuccess = freezed,
  }) {
    return _then(_DynamicFormScreenState(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool?,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool?,
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

class _$_DynamicFormScreenState implements _DynamicFormScreenState {
  const _$_DynamicFormScreenState(
      {this.isLoaded, this.isError, this.isLoading, this.isSuccess});

  @override
  final bool? isLoaded;
  @override
  final bool? isError;
  @override
  final bool? isLoading;
  @override
  final bool? isSuccess;

  @override
  String toString() {
    return 'DynamicFormScreenState(isLoaded: $isLoaded, isError: $isError, isLoading: $isLoading, isSuccess: $isSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DynamicFormScreenState &&
            (identical(other.isLoaded, isLoaded) ||
                const DeepCollectionEquality()
                    .equals(other.isLoaded, isLoaded)) &&
            (identical(other.isError, isError) ||
                const DeepCollectionEquality()
                    .equals(other.isError, isError)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isSuccess, isSuccess) ||
                const DeepCollectionEquality()
                    .equals(other.isSuccess, isSuccess)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoaded) ^
      const DeepCollectionEquality().hash(isError) ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isSuccess);

  @JsonKey(ignore: true)
  @override
  _$DynamicFormScreenStateCopyWith<_DynamicFormScreenState> get copyWith =>
      __$DynamicFormScreenStateCopyWithImpl<_DynamicFormScreenState>(
          this, _$identity);
}

abstract class _DynamicFormScreenState implements DynamicFormScreenState {
  const factory _DynamicFormScreenState(
      {bool? isLoaded,
      bool? isError,
      bool? isLoading,
      bool? isSuccess}) = _$_DynamicFormScreenState;

  @override
  bool? get isLoaded => throw _privateConstructorUsedError;
  @override
  bool? get isError => throw _privateConstructorUsedError;
  @override
  bool? get isLoading => throw _privateConstructorUsedError;
  @override
  bool? get isSuccess => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$DynamicFormScreenStateCopyWith<_DynamicFormScreenState> get copyWith =>
      throw _privateConstructorUsedError;
}
