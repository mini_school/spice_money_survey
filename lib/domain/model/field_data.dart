class FieldData {
/*
{
  "field_id": "XXXXXXXXXX",
  "field_data": "XXXXXXXX"
} 
*/

  String? fieldId;
  String? fieldData;

  FieldData({
    this.fieldId,
    this.fieldData,
  });
  FieldData.fromJson(Map<String, dynamic> json) {
    fieldId = json["field_id"]?.toString();
    fieldData = json["field_data"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["field_id"] = fieldId;
    data["field_data"] = fieldData;
    return data;
  }
}
