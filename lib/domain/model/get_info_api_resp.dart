class GetInfoApiRespFieldsValidations {
/*
{
  "required": true
} 
*/

  bool? required;

  GetInfoApiRespFieldsValidations({
    this.required,
  });
  GetInfoApiRespFieldsValidations.fromJson(Map<String, dynamic> json) {
    required = json["required"];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["required"] = required;
    return data;
  }
}

class GetInfoApiRespFieldsPropertiesChoices {
/*
{
  "label": "Male"
} 
*/

  String? label;

  GetInfoApiRespFieldsPropertiesChoices({
    this.label,
  });
  GetInfoApiRespFieldsPropertiesChoices.fromJson(Map<String, dynamic> json) {
    label = json["label"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["label"] = label;
    return data;
  }
}

class GetInfoApiRespFieldsProperties {
/*
{
  "alphabetical_order": false,
  "choices": [
    {
      "label": "Male"
    }
  ]
  "steps":7
} 
*/

  bool? alphabeticalOrder;
  List<GetInfoApiRespFieldsPropertiesChoices?>? choices;
  int? steps = 0;
  GetInfoApiRespFieldsProperties({
    this.alphabeticalOrder,
    this.choices,
    this.steps,
  });
  GetInfoApiRespFieldsProperties.fromJson(Map<String, dynamic> json) {
    alphabeticalOrder = json["alphabetical_order"];
    steps = json["steps"];
    if (json["choices"] != null) {
      final v = json["choices"];
      final arr0 = <GetInfoApiRespFieldsPropertiesChoices>[];
      v.forEach((v) {
        arr0.add(GetInfoApiRespFieldsPropertiesChoices.fromJson(v));
      });
      choices = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["alphabetical_order"] = alphabeticalOrder;
    data["steps"] = steps;
    if (choices != null) {
      final v = choices;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data["choices"] = arr0;
    }
    return data;
  }
}

class GetInfoApiRespFields {
/*
{
  "id": "Z2sakg81mydT",
  "title": "Enter your gender",
  "properties": {
    "alphabetical_order": false,
    "choices": [
      {
        "label": "Male"
      }
    ]
  },
  "validations": {
    "required": true
  },
  "type": "dropdown"
} 
*/

  String? id;
  String? title;
  GetInfoApiRespFieldsProperties? properties;
  GetInfoApiRespFieldsValidations? validations;
  String? type;

  GetInfoApiRespFields({
    this.id,
    this.title,
    this.properties,
    this.validations,
    this.type,
  });
  GetInfoApiRespFields.fromJson(Map<String, dynamic> json) {
    id = json["id"]?.toString();
    title = json["title"]?.toString();
    properties = (json["properties"] != null)
        ? GetInfoApiRespFieldsProperties.fromJson(json["properties"])
        : null;
    validations = (json["validations"] != null)
        ? GetInfoApiRespFieldsValidations.fromJson(json["validations"])
        : null;
    type = json["type"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = id;
    data["title"] = title;
    if (properties != null) {
      data["properties"] = properties!.toJson();
    }
    if (validations != null) {
      data["validations"] = validations!.toJson();
    }
    data["type"] = type;
    return data;
  }
}

// ignore: camel_case_types
class GetInfoApiRespThankyou_screens {
/*
{
  "title": "Thank you for your feedback, we really appreciate your efforts for filling this response."
} 
*/

  String? title;

  GetInfoApiRespThankyou_screens({
    this.title,
  });
  GetInfoApiRespThankyou_screens.fromJson(Map<String, dynamic> json) {
    title = json["title"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["title"] = title;
    return data;
  }
}

class GetInfoApiRespWelcomeScreen {
/*
{
  "title": "Welcome to personal information  survey. Let's Begin!!",
  "description": "We are collecting information of users visiting our center"
} 
*/

  String? title;
  String? description;

  GetInfoApiRespWelcomeScreen({
    this.title,
    this.description,
  });
  GetInfoApiRespWelcomeScreen.fromJson(Map<String, dynamic> json) {
    title = json["title"]?.toString();
    description = json["description"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["title"] = title;
    data["description"] = description;
    return data;
  }
}

class GetInfoApiResp {
/*
{
  "title": "Personal Information Survey",
  "welcome_screen": {
    "title": "Welcome to personal information  survey. Let's Begin!!",
    "description": "We are collecting information of users visiting our center"
  },
  "thankyou_screens": {
    "title": "Thank you for your feedback, we really appreciate your efforts for filling this response."
  },
  "fields": [
    {
      "id": "a7PPrsreuktH",
      "title": "Enter your name",
      "validations": {
        "required": true
      },
      "type": "short_text"
    }
  ]
} 
*/

  String? title;
  GetInfoApiRespWelcomeScreen? welcomeScreen;
  GetInfoApiRespThankyou_screens? thankyouScreens;
  List<GetInfoApiRespFields?>? fields;

  GetInfoApiResp({
    this.title,
    this.welcomeScreen,
    this.thankyouScreens,
    this.fields,
  });
  GetInfoApiResp.fromJson(Map<String, dynamic> json) {
    title = json["title"]?.toString();
    welcomeScreen = (json["welcome_screen"] != null)
        ? GetInfoApiRespWelcomeScreen.fromJson(json["welcome_screen"])
        : null;
    thankyouScreens = (json["thankyou_screens"] != null)
        ? GetInfoApiRespThankyou_screens.fromJson(json["thankyou_screens"])
        : null;
    if (json["fields"] != null) {
      final v = json["fields"];
      final arr0 = <GetInfoApiRespFields>[];
      v.forEach((v) {
        arr0.add(GetInfoApiRespFields.fromJson(v));
      });
      fields = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["title"] = title;
    if (welcomeScreen != null) {
      data["welcome_screen"] = welcomeScreen!.toJson();
    }
    if (thankyouScreens != null) {
      data["thankyou_screens"] = thankyouScreens!.toJson();
    }
    if (fields != null) {
      final v = fields;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data["fields"] = arr0;
    }
    return data;
  }
}
