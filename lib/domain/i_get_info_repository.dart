import 'package:dartz/dartz.dart';
import 'package:spice_money/spice_money.dart';

abstract class IGetInfoRepository {
  Future<Either<NetworkFailure, GetInfoApiResp>> getInformation();
  Future<Either<NetworkFailure, FormResponse>> sendInformation(String body);
}
