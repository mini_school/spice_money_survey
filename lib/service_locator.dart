import 'package:alice/alice.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:spice_money/application/dynamic_form_screen/dynamic_form_screen_bloc.dart';
import 'package:spice_money/application/welcome_screen/welcome_screen_bloc.dart';

import 'spice_money.dart';

GetIt sLocator = GetIt.instance;

class ServiceLocator {
  static Future<void> init() async {
    await _registerNetwork();
    await _registerRepositories();
    await _registerBloc();
  }

  /// Register all the networking components
  static Future<void> _registerNetwork() async {
    sLocator.registerSingleton<Alice>(Alice(
      showNotification: kDebugMode,
      showInspectorOnShake: true,
      darkTheme: false,
    ));
    sLocator.registerSingleton<Dio>(await _buildDio());
    sLocator.registerSingleton(NetworkApi(sLocator.get<Dio>()));
  }

  static Future<Dio> _buildDio() async {
    BaseOptions options = BaseOptions(
      baseUrl: AppContant.BaseUrl,
      connectTimeout: AppContant.DEFAULT_CONNECTION_TIMEOUT.inMilliseconds,
      receiveTimeout: AppContant.DEFAULT_RECEIVE_TIMEOUT.inMilliseconds,
    );
    options.headers['content-type'] = 'application/json';
    options.headers['Access-Control-Allow-Origin'] = '*';

    final dio = Dio(options)
      ..transformer = FlutterTransformer()
      ..interceptors.add(sLocator.get<Alice>().getDioInterceptor())
      ..interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          error: true,
          compact: true,
          maxWidth: 100));
    //..interceptors.add(LoggingInterceptors());

    return dio;
  }

  // Register all the repositories
  static _registerRepositories() {
    sLocator.registerSingleton(
      GetInfoRepository(
        sLocator.get<NetworkApi>(),
      ),
    );
  }

  // Register all the bloc
  static _registerBloc() {
    sLocator.registerFactory(
      () => WelcomeScreenBloc(
        getInfoRepository: sLocator.get<GetInfoRepository>(),
      ),
    );
    sLocator.registerFactory(
      () => DynamicFormScreenBloc(
        getInfoRepository: sLocator.get<GetInfoRepository>(),
      ),
    );
  }
}
